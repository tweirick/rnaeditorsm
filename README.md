RNAEditorSM is an implementation of RNAEditor in the workflow manager Snakemake. While RNAEditor simplifies analyzing RNA-seq samples from desktop computers, it can be difficult to use from the command-line on a high-performance computing cluster (HPC). Implementing RNAEditor in Snakemake solves these problems and allows for easy parallelization within an HPC queue system. 

Installation Instructions: 

1. Clone or download  RNAEditorSM

2. Install Software.

3. Download Reference Data. 
